@extends('web.layouts.app')

@section('metadata')
<meta name="description" content="{{ $article->meta_description }}" />
	<meta name="keywords" content="{{ $article->meta_kewords }}" />
@endsection

@section('title', $article->title)

@section('content')

<header class="masthead">
	<div class="overlay"></div>
	<div class="container">	
	</div>
</header>

<!-- Post Content -->
<article>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-10 mx-auto">
				<div class="post-heading">
					<h1>{{ $article->title }}</h1>
					{{--<h2 class="subheading">Problems look mighty small from 150 miles up</h2>--}}
					<span class="meta">Posted by
					<a href="">{{ $article->user_name }}</a>
					on {{ $article->created_at->format('F d, Y') }}</span>
				</div>
				{!! $article->content !!}
			</div>
		</div>
	</div>
</article>

@endsection