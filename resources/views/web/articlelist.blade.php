@extends('web.layouts.app')

@section('metadata')
<meta name="description" content="" />
	<meta name="keywords" content="" />
@endsection

@section('title', 'Applicaion')

@section('content')

<!-- Main Content -->
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-8 col-md-10 mx-auto">
			@foreach($response as $value)
			<div class="post-preview">
				<a href="{{ route('web.article', $value->slug) }}">
					<h2 class="post-title">
						{{ $value->title }}
					</h2>
					{{--<h3 class="post-subtitle">
						Problems look mighty small from 150 miles up
					</h3>--}}
				</a>
				<p class="post-meta">Posted by
					<a href="">{{ $value->user_name }}</a>
					on {{ $value->created_at->format('F d, Y') }}</p>
			</div>
			<hr>
			@endforeach
			<div class="clearfix">
				<a class="btn btn-primary float-right" href="">Older Posts &rarr;</a>
			</div>
		</div>
	</div>
</div>
@endsection