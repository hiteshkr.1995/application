@extends('web.layouts.app')

@section('metadata')
<meta name="description" content="" />
	<meta name="keywords" content="" />
@endsection

@section('title', 'Applicaion')

@section('content')

<!-- Page Header -->
<header class="masthead" style="background-image: url('storage/directory1/images/about-bg.jpg')">
	<div class="overlay"></div>
	<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-10 mx-auto">
		<div class="site-heading">
			<h1></h1>
			<span class="subheading"></span>
		</div>
		</div>
	</div>
	</div>
</header>

<!-- Main Content -->
<div class="container">
	<div class="row justify-content-center">
		<!--<div class="col-lg-8 col-md-10 mx-auto">
			@foreach($response as $value)
			<div class="post-preview">
				<a href="{{ route('web.article', $value->slug) }}">
					<h2 class="post-title">
						{{ $value->title }}
					</h2>
					{{--<h3 class="post-subtitle">
						Problems look mighty small from 150 miles up
					</h3>--}}
				</a>
				<p class="post-meta">Posted by
					<a href="">{{ $value->user_name }}</a>
					on {{ $value->created_at->format('F d, Y') }}</p>
			</div>
			<hr>
			@endforeach
			 Pager
			<div class="clearfix">
				<a class="btn btn-primary float-right" href="">Older Posts &rarr;</a>
			</div>
		</div>-->
		<div class="col-sm-6 mb-4">
		    <div class="card">
		      <div class="card-body">
		        <h5 class="card-title">Article</h5>
		        <p class="card-text">Collection of new and meaningfull information of any topic.</p>
		        <a href="{{ route('web.article.list') }}" class="btn btn-primary">Read More</a>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-6 mb-4">
		    <div class="card">
		      <div class="card-body">
		        <h5 class="card-title">Node Js</h5>
		        <p class="card-text">Article and problem solution for Node Js with latest version.</p>
		        <a href="{{ url('') }}" class="btn btn-primary">Read More</a>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-6 mb-4">
		    <div class="card">
		      <div class="card-body">
		        <h5 class="card-title">Laravel</h5>
		        <p class="card-text">Article and problem solution for PHP Laravel with latest version.</p>
		        <a href="{{ url('') }}" class="btn btn-primary">Read More</a>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-6 mb-4">
		    <div class="card">
		      <div class="card-body">
		        <h5 class="card-title">Python</h5>
		        <p class="card-text">Article and problem solution for Python with latest version.</p>
		        <a href="{{ url('') }}" class="btn btn-primary">Read More</a>
		      </div>
		    </div>
		  </div>
		</div>
		</div>
	</div>
</div>
@endsection

@section('bodyPlugins')
<script type="text/javascript">
class UploadAdapter {
    constructor( loader ) {
        // Save Loader instance to update upload progress.
        this.loader = loader;
    }

    upload() {
        // Update loader's progress.
        server.onUploadProgress( data => {
            loader.uploadTotal = data.total;
            loader.uploaded = data.uploaded;
        } ):

        // Return promise that will be resolved when file is uploaded.
        return server.upload( loader.file );
    }

    abort() {
        // Reject promise returned from upload() method.
        server.abortUpload();
    }
}
</script>
@endsection