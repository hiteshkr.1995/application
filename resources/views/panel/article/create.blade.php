@extends('panel.layouts.app')

@section('title', 'Create Article')

@section('plugins')

	<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

@endsection

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<h1 class="h2">Create Article
			@if (session('success'))
				<span class="text-success">
					{{ session('success') }}
				</span>
			@endif
		</h1>
		<div class="btn-toolbar mb-2 mb-md-0">
			<div class="btn-group mr-2">
				<button class="btn btn-sm btn-outline-secondary" disabled>Share</button>
				<button class="btn btn-sm btn-outline-secondary" disabled>Export</button>
			</div>
			<button class="btn btn-sm btn-outline-secondary dropdown-toggle" disabled>
				<span data-feather="calendar"></span>
				This week
			</button>
		</div>
	</div>
	<form class="my-5" method="POST" action="{{ route('article.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="form-group mb-5">
			<div class="form-row">
				<div class="col">
					<input class="form-control form-control-lg" type="text" placeholder="Article Name" name="aricle_name" value="{{ old('aricle_name') }}" autocomplete="off" required>
				</div>
			</div>
		</div>
		<div class="form-group mb-5">
			<div class="form-row">
				<div class="col-md-6">
					<div class="form-label-group">
						<input type="text" id="metaDescription" class="form-control" placeholder="Meta-Description">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-label-group">
						<input type="text" id="metaKeywords" class="form-control" placeholder="Meta-keywords" required="required">
					</div>
				</div>
			</div>
		</div>
		<div class="form-group mb-5">
			<div class="form-row">
				<div class="col">
					<textarea name="article_content" id="editor"></textarea>
				</div>
			</div>
		</div>
		<input type="submit" class="btn btn-primary btn-block" name="submit" value="Create Article">
	</form>
</main>
@endsection

@section('java-script')

	<script type="text/javascript">
		ClassicEditor
			.create( document.querySelector( '#editor' ) )
			.then( editor => {
				console.log( editor );
			} )
			.catch( error => {
				console.error( error );
			} );
	</script>

	{{--@json(old('article_content'))--}}
@endsection