<?php $response = json_decode($response); ?>
@extends('panel.layouts.app')
@section('title', 'Article')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<h1 class="h2">Articles</h1>
		<div class="btn-toolbar mb-2 mb-md-0">
			<div class="btn-group mr-2">
				<button class="btn btn-sm btn-outline-secondary" disabled>Share</button>
				<button class="btn btn-sm btn-outline-secondary" disabled>Export</button>
			</div>
			<button class="btn btn-sm btn-outline-primary" onclick=location.href="{{ route('article.create') }}">
				<span data-feather="plus-square"></span>
				Create Article
			</button>
		</div>
	</div>

	{{--<h2>Secondary Title</h2>--}}
	<div class="table-responsive">
		<table class="table table-striped table-sm">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Updated At</th>
					<th>Show</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($response as $key => $article)
				<tr>
					<td>{{ ++$key }}</td>
					<td>{{ str_limit($article->title_cz, 20, '...') }}</td>
					<td>{{ $article->updated_at }}</td>
					<td><a href="{{ route('article.show', $article->cz) }}" class="btn btn-sm btn-outline-info">Show</a></td>
					<td><a href="{{ route('article.edit', $article->cz) }}" class="btn btn-sm btn-outline-primary">Edit</a></td>
					<td>
						<form action="{{ route('article.destroy', $article->cz) }}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn btn-sm btn-outline-danger" type="submit">Delete</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</main>
@endsection

@section('java-script')
@endsection