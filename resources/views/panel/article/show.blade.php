<!DOCTYPE html>
<html>
<head>
	<title>Article</title>


	<link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<!-- Bootstrap core CSS -->
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://blackrockdigital.github.io/startbootstrap-clean-blog/css/clean-blog.min.css">
</head>
<body>
	<article>
		<div class="container">
		<div class="row">
			<div><a href="{{ route('article.edit', $article->cz) }}" class="btn btn-success">Update Article</a></div>
			<div class="col-lg-8 col-md-10 mx-auto">
			<h1 class="mt-5">{{ $article->title_cz }}</h1>
			{!! $article->content_cz !!}
			</div>
			<div><a href="{{ route('article.create') }}" class="btn btn-primary">Create Article</a></div>
		</div>
		</div>
	</article>
</body>
</html>