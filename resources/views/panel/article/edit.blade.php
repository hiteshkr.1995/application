@extends('panel.layouts.app')

@section('title', 'Update Article')

@section('plugins')

	<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

@endsection

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<h1 class="h2">Update Article
			@if (session('success'))
				<span class="text-success">
					{{ session('success') }}
				</span>
			@endif
		</h1>
		<div class="btn-toolbar mb-2 mb-md-0">
			<div class="btn-group mr-2">
				<button class="btn btn-sm btn-outline-secondary" disabled>Share</button>
				<button class="btn btn-sm btn-outline-secondary" disabled>Export</button>
			</div>
			<button class="btn btn-sm btn-outline-secondary dropdown-toggle" disabled>
				<span data-feather="calendar"></span>
				This week
			</button>
		</div>
	</div>
	<form class="my-5" method="POST" action="{{ route('article.update', $article->cz) }}" enctype="multipart/form-data">
		@csrf
		@method('PUT')
		<div class="form-group mb-5">
			<div class="form-row">
				<div class="col">
					<input class="form-control form-control-lg{{ $errors->has('aricle_name') ? ' is-invalid' : '' }}" type="text" placeholder="Article Name" name="aricle_name" value="{{ old('aricle_name') }}{{ $article->title_cz }}" autocomplete="off">
					@if ($errors->has('aricle_name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('aricle_name') }}</strong>
						</span>
					@endif
				</div>
			</div>
		</div>
		{{--<div class="form-group mb-5">
			<div class="form-row">
				<div class="col-md-6">
					<div class="form-label-group">
						<input type="text" id="firstName" class="form-control" placeholder="First name" required="required">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-label-group">
						<input type="text" id="lastName" class="form-control" placeholder="Last name" required="required">
					</div>
				</div>
			</div>
		</div>--}}
		<div class="form-group mb-5">
			<div class="form-row">
				<div class="col">
					<textarea name="article_content" id="editor">
						{{ $article->content_cz }}
					</textarea>
				</div>
			</div>
		</div>
		<input type="submit" class="btn btn-primary btn-block" name="submit" value="Update Aticle">
	</form>
</main>
@endsection

@section('java-script')

	<script type="text/javascript">
		ClassicEditor
			.create( document.querySelector( '#editor' ) )
			.then( editor => {
				console.log( editor );
			} )
			.catch( error => {
				console.error( error );
			} );
	</script>

@endsection