<main class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="container-fluid" style="z-index: 1001px">
		<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
			<a class="navbar" href="">{{ Auth::user()->name }}</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExample09">
				<ul class="navbar-nav mr-auto">
					{{--<li class="nav-item active">
						<a class="nav-link" href="">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Logout</a>
						<div class="dropdown-menu" aria-labelledby="dropdown09">
							<a class="dropdown-item" href="">Action</a>
							<a class="dropdown-item" href="">Another action</a>
							<a class="dropdown-item" href="">Log Out</a>
						</div>
					</li>--}}

					<li class="nav-item dropdown no-arrow">
						<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-user-circle fa-fw"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
							<a class="dropdown-item" href="">Settings</a>
							<a class="dropdown-item" href="">Activity Log</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
						</div>
					</li>
				</ul>
				{{--<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search" aria-label="Search">
				</form>--}}
			</div>
		</nav>
	</div>
</main>