<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_articles', function (Blueprint $table) {
            $table->bigIncrements('cz');
            $table->string('title_cz', 255)->nullable($value = true);
            $table->string('slug_cz', 275)->nullable($value = true);
            $table->longText('content_cz')->nullable($value = true);
            $table->enum('status_cz', ['Active', 'Inactive'])->default('Inactive');
            $table->integer('add_cz')->nullable($value = true);
            $table->integer('mdf_cz')->nullable($value = true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_articles');
    }
}
