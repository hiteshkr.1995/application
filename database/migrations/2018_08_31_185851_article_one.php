<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticleOne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qa_articles', function (Blueprint $table) {
            $table->longText('meta_description_cz')->after('slug_cz');
            $table->longText('meta_kewords_cz')->after('meta_description_cz');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qa_articles', function($table) {
            $table->dropColumn('meta_description_cz');
            $table->dropColumn('meta_kewords_cz');
        });
    }
}
