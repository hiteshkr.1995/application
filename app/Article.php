<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table = 'qa_articles';
	protected $primaryKey = 'cz';
	protected $fillable = ['title_cz','slug_cz','meta_description_cz','meta_kewords_cz','content_cz','status_cz','add_cz','mdf_cz'];
}