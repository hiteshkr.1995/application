<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use Auth;

class ArticleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//Define Variables
		$response = array();

		$article = Article::orderBy('cz','desc')->get();

		foreach ($article as $key => $value) {
			$response[] = array(
				'cz' => $value->cz,
				'title_cz' => $value->title_cz,
				'updated_at' => $value->updated_at->format('d-M-Y')
			);
		}

		$response = json_encode($response);

		// return $response;
		// return view('panel.article.index', ['response' => $response]);
		return view('panel.article.index')->with('response', $response);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('panel.article.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		// return $request->all();
		$request->validate([
			'aricle_name' => 'required',
			'article_content' => 'required',
		]);

		$name = $request->aricle_name;
		$slug = str_slug( $request->aricle_name, '-');
		$content = $request->article_content;

		$collection = [
			'title_cz' => $name,
			'slug_cz' => $slug,
			'content_cz' => $content,
			'status_cz' => 'Active',
			'add_cz' => Auth::user()->id,
			'mdf_cz' => Auth::user()->id,
		];

		$article = Article::updateOrCreate($collection);

		return redirect()->route('article.show', $article->cz)->with('success', 'Successfully!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$article = Article::find($id);
		return view('panel.article.show')->with('article', $article);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$article = Article::find($id);
		return view('panel.article.edit')->with('article', $article);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'aricle_name' => 'required',
			'article_content' => 'required',
		]);

		$name = $request->aricle_name;
		$slug = str_slug( $request->aricle_name, '-');
		$content = $request->article_content;

		$collection = [
			'title_cz' => $name,
			'slug_cz' => $slug,
			'content_cz' => $content,
			'status_cz' => 'Active',
			'mdf_cz' => Auth::user()->id,
		];

		$article = Article::find($id)->update($collection);

		return redirect()->route('article.show', $id)->with('success', 'Successfully!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Article::destroy($id);
		return redirect()->route('article.index');
	}
}