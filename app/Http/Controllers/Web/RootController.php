<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use Response;

class RootController extends Controller
{
	public function index()
	{
		$response = array();

		$article = Article::select(
			'qa_articles.cz as id',
			'qa_articles.title_cz as title',
			'qa_articles.slug_cz as slug',
			'qa_articles.created_at',
			'users.name as user_name'
		)
		->leftJoin('users', 'qa_articles.mdf_cz', '=', 'users.id')
		->orderBy('qa_articles.cz','desc')->get();

		// return $article[0]->created_at->format('mdy');

		// foreach ($article as $key => $value) {
		// 	$response[] = array(
		// 		'id' => $value->id,
		// 		'title' => $value->title,
		// 		'mdf_by' => $value->user_name,
		// 		'created_at' => $value->created_at->format('F d, Y'),
		// 	);
		// }

		// return $article = json_encode($article, JSON_PRETTY_PRINT);
		// return view('web.index', ['response' => $response]);
		return view('web.index')->with('response', $article);
		// return view('web.index');
	}

	public function showArticle($slug)
	{
		$article = Article::select(
			'qa_articles.cz as id',
			'qa_articles.title_cz as title',
			'qa_articles.slug_cz as slug',
			'qa_articles.meta_description_cz as meta_description',
			'qa_articles.meta_kewords_cz as meta_kewords',
			'qa_articles.content_cz as content',
			'qa_articles.created_at',
			'users.name as user_name'
		)
		->leftJoin('users', 'qa_articles.mdf_cz', '=', 'users.id')
		->where('qa_articles.slug_cz',$slug)
		->first();

		// echo "<pre>";
		// return $article = json_encode($article, JSON_PRETTY_PRINT);
		if (!empty($article)) {
			return view('web.article')->with('article', $article);
		} else {
			abort(404);
		}
	}

	public function articleList($value='')
	{
		$response = array();

		$article = Article::select(
			'qa_articles.cz as id',
			'qa_articles.title_cz as title',
			'qa_articles.slug_cz as slug',
			'qa_articles.created_at',
			'users.name as user_name'
		)
		->leftJoin('users', 'qa_articles.mdf_cz', '=', 'users.id')
		->orderBy('qa_articles.cz','desc')->get();

		// return $article[0]->created_at->format('mdy');

		// foreach ($article as $key => $value) {
		// 	$response[] = array(
		// 		'id' => $value->id,
		// 		'title' => $value->title,
		// 		'mdf_by' => $value->user_name,
		// 		'created_at' => $value->created_at->format('F d, Y'),
		// 	);
		// }

		// return $article = json_encode($article, JSON_PRETTY_PRINT);
		// return view('web.index', ['response' => $response]);
		return view('web.articlelist')->with('response', $article);
		// return view('web.index');
	}
}
