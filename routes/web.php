<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Web Routes
// Route::get('/', function () {
// 	return view('welcome');
// });
Route::get('/', 'Web\RootController@index')->name('root');

Route::get('/about', function () {
	return view('web.about');
})->name('about');

Route::get('/contact', function () {
	return view('web.contact');
})->name('contact');

Route::get('/article/{slug}', 'Web\RootController@showArticle')->name('web.article');
Route::get('/article', 'Web\RootController@articleList')->name('web.article.list');


// Panel Routes
Route::middleware(['auth'])->prefix('panel')->group(function () {
	Route::get('/', 'Panel\HomeController@index')->name('home');
	Route::resources([
		'article' => 'Panel\ArticleController',
	]);
	Route::post('textarea-upload-image', 'Panel\FroalaController@textAreaUploadImage')->name('textAreaUploadImage');
	Route::post('textarea-delete-image', 'Panel\FroalaController@textAreaDeleteImage')->name('textAreaDeleteImage');
});

Auth::routes();